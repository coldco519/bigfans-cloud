package com.bigfans.systemservice.model;

import com.bigfans.systemservice.model.entity.OperatorEntity;
import lombok.Data;

@Data
public class Operator extends OperatorEntity {

	private static final long serialVersionUID = -4074625277326161557L;
	
	/** 注册表单数据 */
	private String verificationCode;
	private String confirmedPassword;
	private Boolean agreeTerms;
	private String returnUrl;

	/* 登录表单数据 */
	private boolean rememberMe;
	
}
