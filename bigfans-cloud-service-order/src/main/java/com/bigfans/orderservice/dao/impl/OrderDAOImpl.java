package com.bigfans.orderservice.dao.impl;

import com.bigfans.framework.dao.BeanDecorator;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.orderservice.dao.OrderDAO;
import com.bigfans.orderservice.model.Order;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @Description:订单Mapper
 * @author lichong 
 * 2014年12月14日下午4:05:14
 *
 */
@Repository(OrderDAOImpl.BEAN_NAME)
public class OrderDAOImpl extends MybatisDAOImpl<Order> implements OrderDAO {

	public static final String BEAN_NAME = "orderDAO";

	@Override
	public Order getByUser(String userId, String orderId) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("id", orderId);
		return getSqlSession().selectOne(className + ".getByUser", params);
	}
	
	@Override
	public int updateStatus(String orderId , String status) {
		Order order = new Order();
		order.setId(orderId);
		order.setStatus(status);
		return new BeanDecorator(order).update();
	}
	
	@Override
	public List<Order> listByUser(String userId , Long start , Long pagesize , boolean pageable) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("pageable", pageable);
		return getSqlSession().selectList(className + ".listByUser", params);
	}
	
}
