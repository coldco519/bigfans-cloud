package com.bigfans.orderservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.orderservice.dao.PayMethodDAO;
import com.bigfans.orderservice.model.PayMethod;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月4日下午9:27:43
 *
 */
@Repository(PayMethodDAOImpl.BEAN_NAME)
public class PayMethodDAOImpl extends MybatisDAOImpl<PayMethod> implements PayMethodDAO {

	public static final String BEAN_NAME = "payTypeDAO";

	@Override
	public PayMethod getByCode(String code) {
		ParameterMap params = new ParameterMap();
		params.put("code", code);
		return getSqlSession().selectOne(className + ".load", params);
	}

	@Override
	public List<PayMethod> listTopMethods() {
		return getSqlSession().selectList(className + ".listTopMethods");
	}

	@Override
	public List<PayMethod> listByType(String type) {
		return null;
	}
}
