package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.BrandDAO;
import com.bigfans.catalogservice.model.Brand;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(BrandDAOImpl.BEAN_NAME)
public class BrandDAOImpl extends MybatisDAOImpl<Brand> implements BrandDAO {

	public static final String BEAN_NAME = "brandDAO";
	
	@Override
	public List<Brand> search(String keyword , String categoryId , Long start , Long pagesize) {
		ParameterMap parameter = new ParameterMap();
		parameter.put("keyword", keyword == null ? "" : keyword);
		parameter.put("categoryId", categoryId);
		return getSqlSession().selectList(className + ".searchservice", parameter);
	}
	

}
