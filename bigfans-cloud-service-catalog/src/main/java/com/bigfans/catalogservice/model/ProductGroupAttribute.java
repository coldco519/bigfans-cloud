package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.ProductGroupAttributeEntity;
import lombok.Data;

@Data
public class ProductGroupAttribute extends ProductGroupAttributeEntity {

	private static final long serialVersionUID = -6474776256442538481L;

	private String value;
	private String optionName;

}
