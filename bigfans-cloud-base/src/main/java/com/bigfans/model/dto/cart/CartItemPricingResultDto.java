package com.bigfans.model.dto.cart;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lichong
 * @create 2018-02-16 下午2:36
 **/
@Data
public class CartItemPricingResultDto {

    private String prodId;
    private Integer quantity;
    private BigDecimal price;
    private BigDecimal originalPrice;
    private BigDecimal subtotal;
    private BigDecimal originalSubTotal;
}
