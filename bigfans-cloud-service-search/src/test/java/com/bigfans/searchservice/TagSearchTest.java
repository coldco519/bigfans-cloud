package com.bigfans.searchservice;

import com.bigfans.framework.exception.ServiceRuntimeException;
import com.bigfans.searchservice.model.Tag;
import com.bigfans.searchservice.service.TagSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author lichong
 * @create 2018-03-17 上午10:26
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchServiceApp.class)
public class TagSearchTest {

    @Autowired
    private TagSearchService tagSearchService;

    @Test
    public void testSearch(){
        try {
            List<Tag> tags = tagSearchService.searchTagByKeyword("kxt", 10);
            System.out.println(tags.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
