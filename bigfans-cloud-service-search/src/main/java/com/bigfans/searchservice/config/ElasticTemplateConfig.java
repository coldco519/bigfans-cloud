package com.bigfans.searchservice.config;

import com.bigfans.framework.es.ElasticConnectionFactory;
import com.bigfans.framework.es.ElasticTemplate;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ElasticTemplateConfig implements EnvironmentAware{
	
	private Environment env;

	@Bean(name="elasticTemplate")
	public ElasticTemplate createTemplate(){
		ElasticConnectionFactory factory = new ElasticConnectionFactory();
		factory.setHost(env.getProperty("elasticsearch.host"));
		factory.setPort(env.getProperty("elasticsearch.port" , Integer.class));
		factory.setCluster(env.getProperty("elasticsearch.cluster"));
		ElasticTemplate elasticTemplate = new ElasticTemplate(factory);
		return elasticTemplate;
	}

	@Override
	public void setEnvironment(Environment env) {
		this.env = env;
	}
	
}
